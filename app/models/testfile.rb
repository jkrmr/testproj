# frozen_string_literal: true

class HelloWorld
  def initialize
    puts "hello, world!"
  end
end